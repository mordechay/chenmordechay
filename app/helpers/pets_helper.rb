module PetsHelper

    def pets_table(pets)
        result = StringIO.new

        columns = ['Name', 'Kind', 'Owner', 'Date of birth']
        result << content_tag(:table) do
          content_tag(:thead) do
            content_tag(:tr) do 
              columns.map {|c| concat content_tag(:th, c)}
            end    
          end
        end
        result.string.html_safe
      end


=begin
<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Kind</th>
      <th>Owner</th>
      <th>Date of birth</th>
      <th colspan="3"></th>
    </tr>
  </thead>

  <tbody>
    <% @pets.each do |pet| %>
        <tr>
          <td><%= pet.name %></td>
          <td><%= pet.kind %></td>
          <td><%= pet.owner_id %></td>
          <td><%= pet.date_of_birth %></td>
          <td><%= link_to 'Show', pet %></td>
          <td><%= link_to 'Edit', edit_pet_path(pet) %></td>
          <td><%= link_to 'Destroy', pet, method: :delete, data: { confirm: 'Are you sure?' } %></td>
        </tr>
    <% end %>
</tbody>
</table>


    
=end


end

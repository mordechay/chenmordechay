class Person < ActiveRecord::Base
    has_many :pets, :foreign_key => 'owner_id'
    has_many :likes
end

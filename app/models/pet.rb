class Pet < ActiveRecord::Base
    belongs_to :person, :class_name => "Person", :foreign_key => 'owner_id'
    has_many :like

    enum kind: [ :dog, :cat, :monkey]
end

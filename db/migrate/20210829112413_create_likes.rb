class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.integer :owner_id
      t.integer :pet_id

      t.timestamps null: false
    end
  end
end

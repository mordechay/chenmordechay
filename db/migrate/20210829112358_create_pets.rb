class CreatePets < ActiveRecord::Migration
  def change
    create_table :pets do |t|
      t.string :name
      t.integer :kind
      t.integer :owner_id
      t.date :date_of_birth

      t.timestamps null: false
    end
  end
end
